const Koa =  require('koa');

const jsonPlaceholder = require('./jsonplaceholder/jsonplaceholder.controller')

const app = new Koa();
app.use(jsonPlaceholder.routes());
app.use(jsonPlaceholder.allowedMethods());

module.exports = app;