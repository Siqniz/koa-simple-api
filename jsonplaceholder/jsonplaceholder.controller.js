const Router = require('koa-router');

const routerOpts = {
    prefix:'/jsonplace'
}

const router = new Router(routerOpts);

router.get('/', async (ctx, next)=>{
    ctx.body = [{name:'Kai', age:31},{name:'Eric', age:44},{name:'Archer', age:12},{name:'Cheryl', age:45},{name:'Tere', age:28}]
    await next()
})

module.exports = router;